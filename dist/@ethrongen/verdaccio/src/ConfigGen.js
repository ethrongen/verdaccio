"use strict";

var _core = require("@dogmalang/core");

var _core2 = require("@ethronjs/core.gen");

const path = _core.dogma.use(require("@dogmalang/path"));

const $ConfigGen = class ConfigGen extends _core2.HandlebarsGen {
  constructor() {
    super(...arguments);
    {}
  }

};
const ConfigGen = new Proxy($ConfigGen, {
  apply(receiver, self, args) {
    return new $ConfigGen(...args);
  }

});
module.exports = exports = ConfigGen;
const Self = ConfigGen;
Object.defineProperty(Self.prototype, "desc", {
  enum: true,
  get: function () {
    {
      return "Generate a config.yaml file.";
    }
  }
});
Object.defineProperty(Self.prototype, "params", {
  enum: true,
  get: function () {
    {
      return {
        ["fileName"]: {
          ["title"]: "File name",
          ["dflt"]: "config.yaml"
        },
        ["pkg"]: "Private page or @scope",
        ["pkgAccess"]: {
          ["title"]: "Package access",
          ["opts"]: ["$all", "$anonymous", "@all", "@anonymous", "all", "undefined", "anonymous"],
          ["dflt"]: "$all"
        },
        ["pkgPublish"]: {
          ["title"]: "Publish access",
          ["opts"]: ["$all", "$anonymous", "@all", "@anonymous", "all", "undefined", "anonymous"],
          ["dflt"]: "$all"
        },
        ["pkgUnpublish"]: {
          ["title"]: "Unpublish access",
          ["opts"]: ["$all", "$anonymous", "@all", "@anonymous", "all", "undefined", "anonymous"],
          ["dflt"]: "$all"
        }
      };
    }
  }
});

Self.prototype.prompt = async function (answers) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("answers", answers, null);

  {
    0, await this.input("fileName");

    if (0, await this.input("pkg")) {
      0, await this.select("pkgAccess");
      0, await this.select("pkgPublish");
      0, await this.select("pkgUnpublish");
    }
  }
};

Self.prototype.pregenerate = async function (answers) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("answers", answers, null);

  {
    {
      const pkg = answers.pkg;

      if (pkg) {
        if (pkg.startsWith("@")) {
          if (!pkg.includes("/") || pkg.endsWith("/")) {
            answers.pkg = pkg + "/*";
          }
        }
      }
    }
  }
};

Self.prototype.generate = async function (answers) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("answers", answers, null);

  {
    0, await this.tmpl.render("config.yaml", answers, answers.fileName);
  }
};