# @ethrongen/verdaccio

[![NPM version](https://img.shields.io/npm/v/@ethrongen/verdaccio.svg)](https://npmjs.org/package/@ethrongen/verdaccio)
[![Total downloads](https://img.shields.io/npm/dt/@ethrongen/verdaccio.svg)](https://npmjs.org/package/@ethrongen/verdaccio)

[Ethron.js](http://ethronlabs.com) generator for [Verdaccio](https://verdaccio.org).

*Developed in [Dogma](http://dogmalang.com), compiled to JavaScript.*

*Engineered in Valencia, Spain, EU by EthronLabs.*

## Commands

```
ethronjs g verdaccio -h
```

- `config` generates the file `config.yaml`.
