//imports
const {cat} = require("ethron");
const babel = require("@ethronjs/plugin.babel");
const eslint = require("@ethronjs/plugin.eslint");
const exec = require("@ethronjs/plugin.exec");
const fs = require("@ethronjs/plugin.fs");
const npm = require("@ethronjs/plugin.npm");

//Package name.
const pkg = require("./package.json").name;

//Who can publish the package on NPM.
const who = "ethrongen";

//catalog
cat.macro("lint", [
  [exec, "dogmac check src test"],
  [eslint, "."],
]).title("Lint source code");

cat.macro("trans-dogma", [
  [fs.rm, "./build"],
  [exec, "dogmac js -o build/src src"],
  [exec, "dogmac js -o build/test/unit test/unit"]
]).title("Transpile from Dogma to JS").hidden(true);

cat.macro("trans-js", [
  [fs.rm, "dist"],
  [babel, "build", `dist/${pkg}/`]
]).title("Transpile from JS to JS").hidden(true);

cat.macro("build", [
  cat.get("lint"),
  cat.get("trans-dogma"),
  cat.get("trans-js"),
  [fs.cp, "package.json", `dist/${pkg}/package.json`],
  [fs.cp, "README.md", `dist/${pkg}/README.md`],
  [fs.cp, "tmpl", `dist/${pkg}/tmpl`],
  [fs.cp, "test/data", `dist/${pkg}/test/data`]
]).title("Build package");

cat.macro("test", `./dist/${pkg}/test/unit`).title("Unit testing");

cat.macro("dflt", [
  cat.get("build"),
  cat.get("test")
]).title("Build and test");

cat.call("pub", npm.publish, {
  who,
  access: "public",
  path: `dist/${pkg}`
}).title("Publish on NPM");

cat.call("install", npm.install, {
  pkg: `dist/${pkg}`
}).title("Install globally the generator");
